FROM node:13

WORKDIR /home/node/app

COPY package.json ./
COPY tsconfig.json ./

RUN yarn install

COPY ./src ./src
COPY ./entrypoint.sh /scripts/entrypoint.sh
RUN ["node", "/home/node/app/node_modules/.bin/tsc"]
RUN ["chmod", "+x", "/scripts/entrypoint.sh"]

EXPOSE 8800

CMD ["/scripts/entrypoint.sh"]