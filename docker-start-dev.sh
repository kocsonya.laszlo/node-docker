#!/bin/bash

docker run \
    --rm \
    -it \
    -v "$(pwd)"/src:/home/node/app/src \
    --name node-docker \
    -p 8800:8800 \
    registry.gitlab.com/kocsonya.laszlo/node-docker yarn dev
